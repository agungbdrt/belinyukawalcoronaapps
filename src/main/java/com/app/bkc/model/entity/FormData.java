package com.app.bkc.model.entity;

import com.app.bkc.model.CommonEntity;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = FormData.TABLE_NAME)
@Data
public class FormData extends CommonEntity {
    public static final String TABLE_NAME = "t_datadiri";

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = TABLE_NAME)
    @TableGenerator(name = TABLE_NAME, table = "T_SEQUENCE", pkColumnName = "SEQ_NAME", pkColumnValue = TABLE_NAME, valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 0)
    @Column(name = "id", length = 11)
    private long id;

    @Column(name = "nama")
    private String nama;
    @Column(name = "nik")
    private String nik;
    @Column(name = "nohp")
    private String nohp;
    @Column(name = "kelurahan")
    private String kelurahan;
    @Column(name = "alamat")
    private String alamat;
    @Column(name = "demam")
    private String demam;
    @Column(name = "batuk")
    private String batuk;
    @Column(name = "sesak")
    private String sesak;
    @Column(name = "tenggorokan")
    private String tenggorokan;
    @Column(name = "sakit_lainnya")
    private String sakitLainnya;
    @Column(name = "riwayat_kontak")
    private String riwayatKontak;
    @Column(name = "negara")
    private String negara;
    @Temporal(TemporalType.DATE)
    @Column(name = "tgl_negara")
    private Date tglNegara;
    @Column(name = "kota")
    private String Kota;
    @Temporal(TemporalType.DATE)
    @Column(name = "tgl_kota")
    private Date tglKota;
    @Temporal(TemporalType.DATE)
    @Column(name = "tgl_kembali")
    private Date tglKembali;
    @Column(name = "transportasi")
    private String transportasi;
    @Column(name = "transit")
    private String transit;
    @Column(name = "nama_petugas")
    private String namaPetugas;
    @Column(name = "no_hp_petugas")
    private String noHpPetugas;
    @Column(name = "jabatan_petugas")
    private String jabatanPetugas;

}
