package com.app.bkc.controller.api;

import com.app.bkc.model.entity.FormData;
import com.app.bkc.service.FormDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/api")
public class ApiController {
    @Autowired
    FormDataService formDataService;

    @PostMapping("/create-data")
    public void createData(@RequestBody FormData formData){
        formDataService.insert(formData);
    }

    @GetMapping("/export")
    public ResponseEntity<InputStreamResource> exportData() throws IOException {
        return formDataService.exportToExcel();
    }
}
