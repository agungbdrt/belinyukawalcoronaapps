package com.app.bkc.controller.webview;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class DashboardController {
    @GetMapping
    public String getHomepage(){
        return "homepage";
    }
    @GetMapping("formPendatang")
    public String getformpendatang(){
        return "form-pendatang";
    }

    @GetMapping("daftarPendatang")
    public String getdaftarpendatang(){
        return "daftar-pendatang";
    }
}
