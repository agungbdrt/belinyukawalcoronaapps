package com.app.bkc.service;

import com.app.bkc.model.entity.FormData;
import com.app.bkc.repository.DataPendatangRepository;
import com.app.bkc.service.Utils.ExcelGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@Service
public class FormDataServiceImpl implements FormDataService {
    @Autowired
    ExcelGenerator excel;

    @Autowired
    DataPendatangRepository dataPendatangRepository;

    @Override
    public void insert(FormData formData) {
        formData.setCreatedBy(1);
        formData.setCreatedOn(new Date());
        dataPendatangRepository.save(formData);
    }

    @Override
    public ResponseEntity<InputStreamResource> exportToExcel() throws IOException {
        List<FormData> formData = dataPendatangRepository.findAll();
        ByteArrayInputStream in = excel.exportExcel(formData);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=Data-pendatang.xlsx");
        System.out.println(formData);
        return ResponseEntity.ok().headers(headers).body(new InputStreamResource(in));
    }
}
