package com.app.bkc.service;

import com.app.bkc.model.entity.FormData;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;

import java.io.IOException;

public interface FormDataService {
    void insert(FormData formData);
    ResponseEntity<InputStreamResource> exportToExcel() throws IOException;
}
