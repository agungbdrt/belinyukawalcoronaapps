package com.app.bkc.service.Utils;

import com.app.bkc.model.entity.FormData;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

@Component
public class ExcelGenerator {

    public ByteArrayInputStream exportExcel(List<FormData> datas) throws IOException {
        String[] columns = {"No","nama lengkap", "No Nik", "No Hp", "Kelurahan/Desa", "Alamat",
                            "Demam(suhu >= 37,5)", "batuk pilek", "sesak nafas", "sakit tenggorokan",
                            "sakit lainnya","riwayat kontak", "kunjungan luar negeri","tgl kunjungan luar negri",
                            "kunjungan dlm negeri", "tgl kunjungan dlm negeri","tgl kembali", "transportasi","transit",
                            "Petugas Input","No.Hp Petugas", "jabatan petugas"};
        try(Workbook workbook = new XSSFWorkbook();
            ByteArrayOutputStream out = new ByteArrayOutputStream()){
            CreationHelper creationHelper = workbook.getCreationHelper();
            Sheet sheet = workbook.createSheet("Data Pendatang");
            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setColor(IndexedColors.BLACK.getIndex());
            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);
            CellStyle dateFormat = workbook.createCellStyle();
            dateFormat.setDataFormat(creationHelper.createDataFormat().getFormat("yyyy-mm-dd"));

            //Row for Header
            Row headerRow = sheet.createRow(0);

            //Header
            for(int i = 0; i<columns.length;i++){
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(columns[i]);
                cell.setCellStyle(headerCellStyle);
            }

            int rowIdx = 1;
            for(FormData data : datas){
                Row row = sheet.createRow(rowIdx);

                row.createCell(0).setCellValue(rowIdx);
                row.createCell(1).setCellValue(data.getNama());
                row.createCell(2).setCellValue(data.getNik());
                row.createCell(3).setCellValue(data.getNohp());
                row.createCell(4).setCellValue(data.getKelurahan());
                row.createCell(5).setCellValue(data.getAlamat());
                row.createCell(6).setCellValue(data.getDemam());
                row.createCell(7).setCellValue(data.getBatuk());
                row.createCell(8).setCellValue(data.getSesak());
                row.createCell(9).setCellValue(data.getTenggorokan());
                row.createCell(10).setCellValue(data.getSakitLainnya());
                row.createCell(11).setCellValue(data.getRiwayatKontak());
                row.createCell(12).setCellValue(data.getNegara());
                Cell cellNegara = row.createCell(13);
                cellNegara.setCellValue(data.getTglNegara());
                cellNegara.setCellStyle(dateFormat);
                row.createCell(14).setCellValue(data.getKota());
                Cell cellKota = row.createCell(15);
                cellKota.setCellValue(data.getTglNegara());
                cellKota.setCellStyle(dateFormat);
                Cell cellKembali = row.createCell(16);
                cellKembali.setCellValue(data.getTglKembali());
                cellKembali.setCellStyle(dateFormat);
                row.createCell(17).setCellValue(data.getTransportasi());
                row.createCell(18).setCellValue(data.getTransit());
                row.createCell(19).setCellValue(data.getNamaPetugas());
                row.createCell(20).setCellValue(data.getNoHpPetugas());
                row.createCell(21).setCellValue(data.getJabatanPetugas());
                rowIdx++;
            }
            workbook.write(out);
            workbook.close();
            return new ByteArrayInputStream(out.toByteArray());
        }catch (Exception e){
            System.out.println(e);
        }
        return null;
    }
}
