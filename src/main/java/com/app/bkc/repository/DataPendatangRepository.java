package com.app.bkc.repository;

import com.app.bkc.model.entity.FormData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DataPendatangRepository extends JpaRepository<FormData, Long> {
}
